# MNT Reform System Image

This is a collection of scripts used to build an image file that can then be transferred to a SD Card using `dd`.

To start the process, use `./mkimage.sh`. To start over, execute `./cleanup.sh` to delete the existing userland and image.

The resulting file is `reform-system.img`.

## License

Copyright 2018-2020 Lukas F. Hartmann / MNT Research GmbH

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details

