#!/bin/bash

for d in dev proc sys
do
	umount -f target-userland/$d || echo "Umounting $d failed"
done

rm -rf target-userland
rm reform-system.img

